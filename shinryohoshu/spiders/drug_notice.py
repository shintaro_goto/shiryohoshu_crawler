import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shinryohoshu.items import *

class DrugNoticeSpider(CrawlSpider):
    name = 'drug_notice'
    allowed_domains = ['shinryohoshu.mhlw.go.jp']
    start_urls = ['http://shinryohoshu.mhlw.go.jp/shinryohoshu/paMenu/doPaListYp']

    def parse_start_url(self, response):
        for row in response.css('div.main2 tr'):
            res = {
                'url'          : row.css('td:nth-child(5) a').attrib.get('href'),
                'desc'         : row.css('td:nth-child(4)::text').get(default='').strip(),
                'published_at' : row.css('td:nth-child(2)::text').get(default='').strip(),
                'available_at' : row.css('td:nth-child(3)::text').get(default='').strip(),
                'count'        : int((row.css('td:nth-child(5) a::text').get(default='0')).strip().replace(',', '')),
                'cookiejar'    : row.css('td:nth-child(5) a').attrib.get('href')
            }
            #yield NoticeIndex(**res)
            if(not res['url']):
                continue
            if(res['desc']!='新規医薬品'):
                continue

            url = 'https://shinryohoshu.mhlw.go.jp' + res['url'].split(';')[0]
            yield scrapy.Request(
                scrapy.utils.url.safe_url_string(url),
                self.parse_detail, meta=res, encoding='cp932')
    
    def parse_detail(self, response):
        for a in response.css('a'):
            href = a.attrib.get('href')
            if href.find('doPaDetailYpNext') < 0:
                continue
            url = 'https://shinryohoshu.mhlw.go.jp' + href + '?' + response.meta['desc'] + response.meta['published_at']
            yield scrapy.Request(
                    scrapy.utils.url.safe_url_string(url),
                    self.parse_detail, meta=response.meta, encoding='cp932')

        for row in response.css('div.main2 tr'):
            res = {
                'name'              : row.css('td:nth-child(1)::text').get(default='').strip(),
                'unit'              : row.css('td:nth-child(2)::text').get(default='').strip(),
                'price'             : row.css('td:nth-child(3)::text').get(default='').strip(),
                'efficacy_class'    : row.css('td:nth-child(4)::text').get(default='').strip(),
                'expiration_date'   : row.css('td:nth-child(5)::text').get(default='').strip(),
                'restriction'       : row.css('td:nth-child(6)::text').get(default='').strip(),
                'registration_form' : row.css('td:nth-child(7)::text').get(default='').strip(),
                'receipt_code'      : row.css('td:nth-child(8)::text').get(default='').strip(),
                'desc'              : response.meta['desc'],
                'published_at'      : response.meta['published_at'],
                'available_at'      : response.meta['available_at'],
            }
            if(not res['name']): # empty row
                continue
            if(res['name']=='品名'): # header row
                continue
            yield NoticeDetail(**res)