# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ShinryohoshuItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class NoticeIndex(scrapy.Item):
    url = scrapy.Field()
    desc = scrapy.Field()
    published_at = scrapy.Field()
    available_at = scrapy.Field()
    count = scrapy.Field()

class NoticeDetail(scrapy.Item):
    name = scrapy.Field()
    unit = scrapy.Field()
    price = scrapy.Field()
    efficacy_class = scrapy.Field()
    expiration_date = scrapy.Field()
    restriction = scrapy.Field()
    registration_form = scrapy.Field()
    receipt_code = scrapy.Field()
    desc = scrapy.Field()
    published_at = scrapy.Field()
    available_at = scrapy.Field()